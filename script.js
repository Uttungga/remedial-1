// pop-up add user form
document.getElementById("formAdd").style.display = "none";
var addUser_popup = document.getElementById("tambahPengguna");
addUser_popup.onclick = function () {
  document.getElementById("formEdit").style.display = "none";
  document.getElementById("formAdd").style.display = "block";
  // alert();
};

//close add user form
var close_popup = document.getElementById("close");
close_popup.onclick = function () {
  document.getElementById("formAdd").style.display = "none";
};

//pop-up edit user form - first
document.getElementById("formEdit").style.display = "none";

var data_user = [
  { nama: "SUNARDI", ktp: "3402140110990007", id: 0 },
  { nama: "JAIMAN", ktp: "8763402128778899", id: 1 },
  { nama: "KUMALA PUTRI", ktp: "3402140110990098", id: 2 },
  { nama: "TIARA ANDIRA", ktp: "3402140110990009", id: 3 },
  { nama: "AMANDA TYA", ktp: "3402140110990087", id: 4 },
  { nama: "ANGGARA RESTU", ktp: "3402140110990064", id: 5 },
];

// tampil data table
function Displaytable() {
  var html = "<table id='table' class='table table-bordered table-hover table-striped'>";
  html += "<thead class='table-dark'>";
  html += "<tr>";
  html += "<td>" + "Nama" + "</td>";
  html += "<td>" + "No KTP" + "</td>";
  html += "<td class='text-center'>" + "Action" + "</td>";
  html += "</tr>";
  html += "</thead>";
  html += "<tbody>";
  for (var i = 0; i < data_user.length; i++) {
    // var sno = i + 1;
    html += "<tr>";
    // html += "<td hidden >" + sno + "</td>";
    html += "<td id='namaTable'>" + data_user[i].nama + "</td>";
    html += "<td id='ktpTable'>" + data_user[i].ktp + "</td>";
    html +=
      "<td class='text-center'>" +
      `<button class='btn btn-outline-secondary btn-light' onclick='editform(${data_user[i].id})'>
      <i class="bi bi-pen"></i>
      </button>
      <button class='btn btn-outline-secondary btn-light' onclick='removeData(${data_user[i].id})'>
      <i class="bi bi-trash"></i>
      </button>` +
      "</td>";
    html += "</tr>";
  }
  html += "</tbody>";
  html += "</table>";
  document.getElementById("table").innerHTML = html;
}

// search data table
function search() {
  var input = document.getElementById("cari").value.toUpperCase().toString();

  var cek = data_user.find((item) => item.nama.includes(input) || item.ktp.includes(input));

  if (input !== "") {
    var html = "<table id='table' class='table table-bordered table-hover table-striped'>";
    html += "<thead class='table-dark'>";
    html += "<tr>";
    html += "<td>" + "Nama" + "</td>";
    html += "<td>" + "No KTP" + "</td>";
    html += "<td class='text-center'>" + "Action" + "</td>";
    html += "</tr>";
    html += "</thead>";
    html += "<tr>";
    html += "<td id='namaTable'>" + cek.nama + "</td>";
    html += "<td id='ktpTable'>" + cek.ktp + "</td>";

    html +=
      "<td class='text-center'>" +
      `<button class='btn btn-outline-secondary btn-light' onclick='editform(${cek.id})'>
    <i class="bi bi-pen"></i>
    </button>
    <button class='btn btn-outline-secondary btn-light' onclick='removeData(${cek.id})'>
    <i class="bi bi-trash"></i>
    </button>` +
      "</td>";
    console.log(cek.id);
    html += "</tr>";
    document.getElementById("cari").value = "";

    html += "</table>";
    document.getElementById("table").innerHTML = html;
    console.log("kondisi 1");
  } else if (input == "") {
    Displaytable();
    console.log("kondisi 2");
  } else {
    alert("tidak ditemukan");
    console.log("kondisi 3");
  }
  // Displaytable();
}
// var data_user2 = [];

// add data user
function addUserForm() {
  // alert("test..");
  var id = data_user.length;
  var nama = document.getElementById("nama").value.toUpperCase();
  var ktp = parseInt(document.getElementById("ktp").value);
  var data = { nama: nama, ktp: ktp, id: id };

  data_user.push(data);
  Displaytable();
  console.log(data_user);
  document.getElementById("nama").value = "";
  document.getElementById("ktp").value = "";
}

// delete data user
function removeData(rec) {
  // console.log(rec);
  data_user.filter((a, i) => {
    if (rec == a.id) {
      data_user.splice(i, 1);
      Displaytable();
    } else {
      console.log("none");
    }
  });
  console.log(data_user);
  document.getElementById("formEdit").style.display = "none";
}

// ambil data tabel untuk edit
function editform(id) {
  // popup edit section form
  // document.getElementById("formAdd").style.display = "none";
  document.getElementById("formEdit").style.display = "block";
  document.getElementById("closeUpdate").addEventListener("click", () => {
    document.getElementById("formEdit").style.display = "none";
  });

  // var cek = data_user.find((item) => item.id.includes(id));
  var nama = (document.getElementById("namaTable").value = data_user[id].nama);
  var noktp = (document.getElementById("ktpTable").value = data_user[id].ktp);

  console.log(nama, noktp);
  document.getElementById("namaUpdate").value = nama;
  document.getElementById("ktpUpdate").value = noktp;
}

//masukan nilai edit
var update = document.getElementById("saveUpdate");
update.onclick = function (id) {
  var nama = document.getElementById("namaUpdate").value;
  var ktp = document.getElementById("ktpUpdate").value;

  var data = { nama: nama, ktp: ktp, id: id };

  data_user[id].push(data);
  Displaytable();
  console.log(nama, ktp);
};

// function editAdd() {
//   var nama = document.getElementById("namaUpdate").value;
//   var ktp = parseInt(document.getElementById("ktpUpdate").value);
//   var data = { nama: nama, ktp: ktp };
//   // console.log(nama);

//   var updateData = data_user.findIndex((f) => f.id === id);
//   data_user[updateData] = data;
//   console.log(data_user);

//   document.getElementById("namaUpdate").value = "";
//   document.getElementById("ktpUpdate").value = "";
//   // data_user.id = nama;
// }
